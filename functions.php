<?php
/**
 * Enqueues scripts and styles.
 *
 */
function schnexagon_scripts() {
	// Bootstrap stylesheet
	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css' );

	// FontAwesome stylesheet
	wp_enqueue_style( 'fontawesome-style', get_template_directory_uri() . '/css/font-awesome.min.css' );

	// Theme stylesheet
	wp_enqueue_style( 'schnexagon-style', get_stylesheet_uri() );

	// Bootstrap scripts
	wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '20170215', true );

	// Theme scripts
	wp_enqueue_script( 'schnexagon-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20170215', true );
}
add_action( 'wp_enqueue_scripts', 'schnexagon_scripts' );


// Registering the menus
register_nav_menus( array(
	'main' => __( 'Main Menu', 'schnexagon' ),
	'sub' => __( 'Subpage Menu', 'schnexagon' ),
	'footer' => __( 'Footer Menu', 'schnexagon' ),
) );


// Registering the widgets
function schnexagon_widgets_init() {
	register_sidebar( array(
		'name'          => 'Footer Column 1',
		'id'            => 'footer-column-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	register_sidebar( array(
		'name'          => 'Footer Column 2',
		'id'            => 'footer-column-2',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	register_sidebar( array(
		'name'          => 'Footer Column 3',
		'id'            => 'footer-column-3',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	register_sidebar( array(
		'name'          => 'Footer Column 4',
		'id'            => 'footer-column-4',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	register_sidebar( array(
		'name'          => 'Footer Social Media',
		'id'            => 'footer-social-media',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
}
add_action( 'widgets_init', 'schnexagon_widgets_init' );


// Shortcode: Grid System - Row
function schnexagon_shortcode_row( $atts, $content = null ) {

	// puzzle markup
	$html = '<div class="row">' . do_shortcode($content) . '</div>';

	// return the markup
	return $html;
}
add_shortcode( 'row', 'schnexagon_shortcode_row' );


// Shortcode: Grid System - Columns
function schnexagon_shortcode_column( $atts, $content = null ) {

	// attributes
	extract(shortcode_atts(
		array(
			'size' => 6
		), $atts )
	);

	// puzzle markup
	$html = '<div class="col-md-'.$size.'">' . wpautop(do_shortcode($content)) . '</div>';

	// return the markup
	return $html;
}
add_shortcode( 'column', 'schnexagon_shortcode_column' );


// Remove WP version number
function schnexagon_remove_version() {
	return '';
}
add_filter('the_generator', 'schnexagon_remove_version');


// Add ACF Theme Options
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

// Register Google Api-Key for ACF Pro
function schnexagon_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyC_HX8byPQ970h-J1fEOeNFGRfMYE3QbFg');
}
add_action('acf/init', 'schnexagon_acf_init');

/**
 * Filter the CSS class for a nav menu based on a condition.
 *
 * @param array  $classes The CSS classes that are applied to the menu item's <li> element.
 * @param object $item    The current menu item.
 * @return array (maybe) modified nav menu class.
 */
function schnexagon_special_nav_class( $classes, $item ) {
    //if ( is_single() && 'Blog' == $item->title ) {
        // Notice you can change the conditional from is_single() and $item->title
        $classes[] = "";
    //}
    return $classes;
}
add_filter( 'nav_menu_css_class' , 'schnexagon_special_nav_class' , 10, 2 );