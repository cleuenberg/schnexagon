/* Custom JS Functions */
jQuery(document).ready(function($) {
	//fixed sidebar navigation
	$('#navbar-left').affix({
		offset: {
			top: 190,
			bottom: function () {
				return (this.bottom = $('.footer').outerHeight(true))
			}
		}
	})
});