<?php
/**
 * The template for displaying the footer
 */
?>
			</div><!-- /.col-md-8 -->
		</div><!-- /.row -->
    </div><!-- /.container -->

    <footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<?php if ( is_active_sidebar( 'footer-column-1' ) ) : ?>
						<?php dynamic_sidebar( 'footer-column-1' ); ?>
					<?php endif; ?>
				</div>
				<div class="col-md-3">
					<?php if ( is_active_sidebar( 'footer-column-2' ) ) : ?>
						<?php dynamic_sidebar( 'footer-column-2' ); ?>
					<?php endif; ?>
				</div>
				<div class="col-md-3">
					<?php if ( is_active_sidebar( 'footer-column-3' ) ) : ?>
						<?php dynamic_sidebar( 'footer-column-3' ); ?>
					<?php endif; ?>
				</div>
				<div class="col-md-3">
					<?php if ( is_active_sidebar( 'footer-column-4' ) ) : ?>
						<?php dynamic_sidebar( 'footer-column-4' ); ?>
					<?php endif; ?>
				</div>
			</div>
			<?php if ( is_active_sidebar( 'footer-social-media' ) ) : ?>
			<div class="row">
				<div class="col-md-12 text-center">
					<?php dynamic_sidebar( 'footer-social-media' ); ?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</footer>

<?php wp_footer(); ?>
</body>
</html>