<?php
/**
 * The template for displaying the header
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="icon" type="image/png" href="<?php echo get_bloginfo('url'); ?>/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_bloginfo('url'); ?>/favicon-16x16.png" sizes="16x16">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<meta name="google-site-verification" content="fxDHZgLR6wDW87SPXNiDSoqpfwUdp68mqapwCnOZSlI">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-spy="scroll" data-target="#navbar-left">

	<div class="header container-fluid">
		<div class="container">

			<?php if ( has_nav_menu( 'main' ) ) : ?>
			<nav class="navbar" id="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php if( get_field('logo_link', 'option') ): the_field('logo_link', 'option'); else: echo get_bloginfo('url'); endif;  ?>">
						<img alt="<?php echo get_bloginfo('name'); ?>" src="<?php the_field('logo', 'option'); ?>" class="img-responsive">
					</a>
				</div>

				<div class="collapse navbar-collapse" id="navbar-collapse-1">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'main',
							'menu_class'     => 'nav navbar-nav',
						));
					?>
				</div>
			</nav>
			<?php endif; ?>

		</div>
	</div>

	<div class="container">

		<div class="row">
			<div class="col-md-12">